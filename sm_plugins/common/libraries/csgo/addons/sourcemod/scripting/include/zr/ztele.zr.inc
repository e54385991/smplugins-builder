/**
 * Force teleport client to spawn point, client must be alive.
 *
 * @param client	Client index.
 */
native void ZR_TeleForce(int client);