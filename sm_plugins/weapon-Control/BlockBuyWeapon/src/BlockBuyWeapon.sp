#include <sourcemod>
#include <cstrike>

#pragma semicolon 1  

//Just Block Buy
int g_iBlockWeaponListCount = 0;
char g_sBlockWeaponList[10][32];

ConVar g_cvarBlockList;

char WeaponBlockBuy[][] = { 
	"g3sg1",  
	"scar20",  
};

public void OnPluginStart()
{
	g_cvarBlockList = CreateConVar("sm_blockbuy_weapons", "", "blocklist");
	g_cvarBlockList.AddChangeHook(OnConVarChanged);
	AutoExecConfig(true);
}

public void OnConVarChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
	if(convar == g_cvarBlockList)
	{
		g_iBlockWeaponListCount = ExplodeString(newValue, ",", g_sBlockWeaponList, sizeof(g_sBlockWeaponList), sizeof(g_sBlockWeaponList[]));
	}
}

public void OnConfigsExecuted()
{
	char sBuffer[255];
	g_cvarBlockList.GetString(sBuffer, sizeof(sBuffer));
	g_iBlockWeaponListCount = ExplodeString(sBuffer, ",", g_sBlockWeaponList, sizeof(g_sBlockWeaponList), sizeof(g_sBlockWeaponList[]));
}

public Action CS_OnBuyCommand(int client, const char[] weapon)
{
	if(g_iBlockWeaponListCount > 0)	
	{
		for(int i = 0; i < g_iBlockWeaponListCount; i++)
		{
			if(strlen(g_sBlockWeaponList[i]) > 1 && g_sBlockWeaponList[i][0] != '\0' && StrContains(g_sBlockWeaponList[i],weapon,false) != -1)
			{
				return Plugin_Handled;  
			}
		}
	}
	
	for(int i=0;i<sizeof(WeaponBlockBuy);i++){ 
		if (StrContains(weapon, WeaponBlockBuy[i],false) != -1){ 
			return Plugin_Handled;  
		} 
	}
	return Plugin_Continue;
}
