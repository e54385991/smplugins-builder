import argparse
import json
import os
import shutil
import subprocess
from subprocess import PIPE
import sys
import tempfile
import time
import copy

class ComplieError(Exception):
    pass

# Set console language to english to avoid encoding issues
os.system('chcp 437')

is_sp_file = lambda fn: any(fn.endswith(ext) for ext in [".sp"])

def newline():
    if not opt.slient:
        print('')

def newproject(proj, group):
    if group is '':
        group = 'common'

    print('Creating a new project "{}" in group "{}"...'.format(proj, group))
    projdir = os.path.join(group, proj)
    os.makedirs(projdir)
    dir_trees = [
        "src",
        
        "csgo/addons/sourcemod/configs",
        "csgo/addons/sourcemod/gamedata",
        "csgo/addons/sourcemod/scripting/include",
        "csgo/addons/sourcemod/translations",
    ]

    for name in dir_trees:
        p = os.path.join(projdir, name)
        print('Creating folder: {}'.format(p))
        os.makedirs(p)

    with open(os.path.join(projdir, "config.json"), 'w') as f:
        configs = {}
        configs["project"] = proj
        configs["targets"] = [{'{}.sp'.format(proj): '{}.smx'.format(proj)}]
        json.dump(configs, f, indent=4)

    print('"{}:{}" was created.'.format(group, proj))

def change_ext(name, ext):
    return os.path.splitext(name)[0] + ext

def runcmd(cmd, cwd=None):
    returncode = -1
    try:
        if sys.version_info[0] == 2:
            proc = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE, cwd=cwd)
        else:
            proc = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE, cwd=cwd, text=True)

        for line in iter(proc.stdout.readline, ''):
            print(line.rstrip())

        returncode = proc.wait()
    except Exception as err:
        print('Error with command "{}": {}'.format(cmd, str(err)))
        return -1
    else:
        return returncode

def popcmd(cmd, cwd=None):
    try:
        proc = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE, cwd=cwd)
        proc.communicate()
    except Exception as err:
        print('Error with command "{}": {}'.format(cmd, str(err)))
        return -1
    else:
        return proc.returncode

def copydir(src, dest):
    cmd('xcopy /E /Y "{}\\*" "{}"'.format(src, dest))

def build_workdir(name):
    targetdir = tempfile.mkdtemp(prefix='xbuild_{}_'.format(name))
    dir_trees = [
        "csgo/addons/sourcemod/plugins",
    ]

    for name in dir_trees:
        os.makedirs(os.path.join(targetdir, name))

    global temp_dir
    temp_dir += [targetdir]
    return targetdir

def copyres(project, resdir):
    path, config = project
    resources = os.path.join(path, 'csgo')
    if os.path.exists(resources):
        print("[{}] copying...".format(config['project']))
        copydir(resources, resdir)
    else:
        print("[{}] No custom resources".format(config['project']))

def complie(complier, workdir, source, includes, args):
    if isinstance(includes, str):
        includes = [includes]
    elif not isinstance(includes, list):
        print("Invaild include path was given: {} ({})".format(str(includes), includes.__class__))
        return -1

    incarg = ''
    for path in includes:
        incarg += ' -i "{}"'.format(path)

    return cmd('{} {} {} {}'.format(
        os.path.join(complier['path'], 'spcomp.exe'), source, incarg, ' '.join(args)), workdir)

def pack(src, target):
    cmd('"{}" a -tzip -mx0 -y -bb0 -bd "{}" {}'.format(zip_path, target, src))
    newline()

def clean():
    print("Remove all temporary directories...")
    for path in temp_dir:
        print("Removing {}".format(path))
        shutil.rmtree(path)

def complie_project(project, targetdir, includes):
    path, config = project
    start = time.time()
    errors = 0
    proj = config['project']
    add_args = config['args'] if 'args' in config else []
    srcdir = os.path.join(path, 'src')

    if 'noBuild' in config and config['noBuild']:
        print("[{}] No build target...skip\n".format(proj))
        return

    private_include = os.path.abspath(os.path.join(path, 'include'))
    if os.path.exists(private_include):
        includes += [private_include]
    
    if opt.job_id is not '':
        add_args += ['BUILD_ID={}'.format(opt.job_id)]

    sources = []
    if 'targets' in config and isinstance(config['targets'], list):
        print('[{}] Found {} build targets'.format(proj, len(config['targets'])))
        for target in config['targets']:
            sources += [tuple(*target.items())]
    else:
        print('[{}] No target was found. Build all .sp files'.format(proj))
        
        spfiles = [x for x in os.listdir(srcdir) if is_sp_file(x)]
        print('[{}] Found {} .sp file'.format(proj, len(spfiles)))

        for sp in spfiles:
            name = os.path.basename(sp)
            sources += [(sp, change_ext(name, '.smx'))]

    sm = default_toolchain
    if 'toolchain' in config:
        if config['toolchain'] in compliers:
            sm = config['toolchain']
        else:
            print('[{}] No toolchain "{}" was found, using default toolchain'.format(proj, sm))

    print('[{}] Using toolchain "{}"'.format(proj, sm))
    for i, src in enumerate(sources):
        if i != 0: newline()
        print('[{}] Compling "{}" => "{}"'.format(proj, *src))
        args = [
            '-o "{}"'.format(os.path.join(targetdir, 'csgo/addons/sourcemod/plugins', src[1]))
        ]
        args += add_args

        ret = complie(compliers[sm], srcdir, src[0], includes, args)
        if ret != 0:
            errors += 1
            if not opt.ignore_error:
                raise ComplieError('[{}] Error when compiling "{}". Return code: {}.'.format(proj, src[0], ret))

    print('[{}] Finished with {} errors in {:.2f}s\n'.format(proj, errors, time.time() - start))

def buildgroup_helper(name, includes):
    group = buildgroups[name]
    print('[{}] Building plugins group: {} projects'.format(name, len(group)))
    try:
        ttime = time.time()
        targetdir = build_workdir(name)
        resdir = os.path.join(targetdir, 'csgo')
        includes += [os.path.join(targetdir, 'csgo/addons/sourcemod/scripting/include')]
        
        print('[{}] Copy reouces to {}'.format(name, resdir))
        for project in group:
            copyres(project, resdir)

        print('\n[{}] Compiling all projects...'.format(name))
        for project in group:
            complie_project(project, targetdir, copy.deepcopy(includes))

        print('\n[{}] Finish compiling plugins group'.format(name))
        
        jobid = opt.job_id
        if jobid == '':
            jobid = "nojobid"

        package = '{}_{}.zip'.format(name, jobid)
        print('[{}] Start packing group to {}'.format(name, package))
        pack(os.path.join(targetdir, 'csgo'), os.path.join(opt.targetdir, package))
        print('[{}] Pack group done.'.format(name))
        
        print('[{}] Build group finished in {:.2f}'.format(name, time.time() - ttime))
        return targetdir
    except Exception as err:
        print(str(err) + '\nXBuild Exiting...')
        clean()
        exit(1)

temp_dir = []
parser = argparse.ArgumentParser(description='XNet Sourcepawn Builder')

parser.add_argument('--new-project', '-n', type=str, default='', help='Create a new Xbuild project')

parser.add_argument('--debug', action='store_true', help='Enable xbuild debug mode')
parser.add_argument('--slient', action='store_true', help='Disable all output by external program')

parser.add_argument('--group', type=str, default='', help='Build which plugins group')
parser.add_argument('--config', type=str, default='xbuild.json', help='')

parser.add_argument('--skip-common', action='store_true', help='Skip the building of common group')
parser.add_argument('--ignore-error', action='store_true', help='Ignore all errors and continue during compling')

parser.add_argument('--targetdir', type=str, default='', help='Target output path')
parser.add_argument('--job-id', type=str, default='', help='The pipline job id for plugins version')

opt = parser.parse_args()

if opt.new_project is not '':
    newproject(opt.new_project, opt.group)
    exit(0)

ttime = time.time()

default_toolchain = ''
zip_path = ''

# check config
compliers = {}
with open(opt.config) as f:
    xbuild_config = json.load(f)

    print('Available toolchain:')
    for toolchain in xbuild_config['toolchain']:
        print('\t{}'.format(toolchain['name']))
        compliers[toolchain['name']] = toolchain

        if 'isDefault' in toolchain and toolchain['isDefault']:
            default_toolchain = toolchain['name']

    print('')
    if len(compliers) == 0:
        print("\tNo available toolchain...exiting")
        exit(1)

    if default_toolchain == '':
        default_toolchain = compliers.items()[0]

    print('Default toolchain: "{}"'.format(default_toolchain))

    zip_path = xbuild_config['7zip']
    if not os.path.exists(zip_path):
        print('Invaild 7zip was given: {}"'.format(zip_path))
        exit(1)

if opt.job_id == '':
    print('Job ID: N/A')
else:
    print('Job ID: {}'.format(opt.job_id))

cmd = popcmd if opt.slient else runcmd

print('\nLoading all build groups...')

buildgroups = {}
groupdirs = [os.path.join('.', x) for x in os.listdir('.') if os.path.isdir(x)]
for g in groupdirs:
    group = os.path.basename(g)
    projdirs = [os.path.join(g, x) for x in os.listdir(g) if os.path.isdir(os.path.join(g, x))]
    print('Find {} projects in build group "{}"'.format(len(projdirs), group))

    for dirs in projdirs:
        projconf = [os.path.join(dirs, x) for x in os.listdir(dirs) if x == 'config.json']
        if len(projconf) == 0:
            print('No "config.json" in "{}"...Skip'.format(dirs))
            continue
        else:
            projconf = projconf[0]

        try:
            f = open(projconf)
            config = json.load(f)

            config['project'] = '{}:{}'.format(group, config['project'])
            print('Loading {}...'.format(config['project']))

            if group in buildgroups:
                buildgroups[group] += [(os.path.dirname(projconf), config)]
            else:
                buildgroups[group] = [(os.path.dirname(projconf), config)]

            f.close()
        except Exception as err:
            print('Error loading "{}": {}'.format(projconf, str(err)))

    print('')

build_all = opt.group == ''
print('Loaded {} groups'.format(len(buildgroups)))
for name,v in buildgroups.items():
    print('\t "{}"'.format(name))

print('')
print('Building "{}" plugins group\n'.format('All' if build_all else opt.group))

extra_includes = []

# Build 'common' group first
if 'common' in buildgroups and not opt.skip_common:
    common_target = buildgroup_helper('common', extra_includes)
    extra_includes += [os.path.join(common_target, 'csgo/addons/sourcemod/scripting/include')]
else:
    print('Skiping "common" build group ...')

if 'common' in buildgroups:
    buildgroups.pop('common')

print('')
for name, proj_list in buildgroups.items():
    if build_all or name == opt.group:
        buildgroup_helper(name, extra_includes)

print('')
clean()
print('\nXBuild finished in {:.2f}s'.format(time.time() - ttime))
